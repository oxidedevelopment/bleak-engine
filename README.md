# README #

The Bleak Engine is a simple engine for Indie/Hobby game developers coding in java.

### What is this repository for? ###

* Indie/Hobby game developers looking for a simple engine.
* A starting point for their own engine.

### How do I get set up? ###

* Go to the downloads page and download the BleakEngine.jar
* Link the engine to your project.

### Contribution guidelines ###

* Make sure your code is neat.
* Please make sure your contribution doesn't majorly affect the engine, or breaks it.
* Please make a well-written commit message(Or comments) about what you did.

### Who do I talk to? ###

* You can create an issue.
