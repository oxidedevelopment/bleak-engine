package com.oxidevelopment.bleakengine.display;

import java.nio.ByteBuffer;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class DisplayManager {

	/**
	 * Set the window size.
	 * @param width Width of window.
	 * @param height Height of window.
	 * @throws LWJGLException Error creating the display size.
	 */
	public static void setWindowSize(int width, int height) throws LWJGLException {
		Display.setDisplayMode(new DisplayMode(width, height));
	}

	/**
	 * Creates the display with the size given.
	 * @throws LWJGLException Error creating the display.
	 */
	public static void createWindow() throws LWJGLException {
		Display.create();
	}
	
	/**
	 * Sets the icon on the taskbar, and top left.
	 * @param icon The icon in a byte buffer.
	 */
	public static void setIcon(ByteBuffer[] icon) {
		Display.setIcon(icon);
	}
	
	/**
	 * Gets the width of the window.
	 * @return The width of the window.
	 */
	public static int getWidth() {
		return Display.getWidth();
	}
	
	/**
	 * Gets the height of the window.
	 * @return the height of the window.
	 */
	public static int getHeight(){
		return Display.getHeight();
	}
	
	/**
	 * Destroy the window.
	 */
	public static void destroy() {
		Display.destroy();
	}

}
