package com.oxidevelopment.bleakengine;

import org.lwjgl.LWJGLException;

import com.oxidevelopment.bleakengine.display.DisplayManager;

public abstract class Game {

	/**
	 * This will create the window and start the game.
	 * 
	 * @throws LWJGLException
	 */
	public void createWindow(int width, int height) throws LWJGLException {
		DisplayManager.setWindowSize(width, height);
		DisplayManager.createWindow();
		GameManager.startGame();
	}

	/**
	 * Setup the display and the things you need, files, networking, etc.
	 */
	public abstract void setup();

	/**
	 * Use SceneManager.setScene() and set the scene, then call createWindow().
	 */
	public abstract void createScene();

}
