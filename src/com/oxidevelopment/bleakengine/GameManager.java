package com.oxidevelopment.bleakengine;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import com.oxidevelopment.bleakengine.display.DisplayManager;
import com.oxidevelopment.bleakengine.scene.Scene;
import com.oxidevelopment.bleakengine.scene.utils.SceneManager;

public class GameManager {

	/**
	 * Starts the game, and draws everything to the screen.
	 */
	public static void startGame() {
		initGL();
		
		while (!Display.isCloseRequested()) {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); 
			Scene scene = SceneManager.getScene();
			scene.update();
			scene.draw();
		}
		
		DisplayManager.destroy();
	}
	
	public static void initGL(){
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, DisplayManager.getWidth(), 0, DisplayManager.getHeight(), 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}

}
