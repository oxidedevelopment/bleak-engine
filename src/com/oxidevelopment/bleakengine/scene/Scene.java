package com.oxidevelopment.bleakengine.scene;

public abstract class Scene {
	
	/**
	 * Load any textures you need, files, etc.
	 */
	public abstract void loadContent();
	/**
	 * Update the game with keyboard input, etc.
	 */
	public abstract void update();
	/**
	 * Draw textures, sprites, and text.
	 */
	public abstract void draw();
	/**
	 * Get rid of anything that has to be destroyed. Probably wont need this.
	 */
	public abstract void dispose();

}
