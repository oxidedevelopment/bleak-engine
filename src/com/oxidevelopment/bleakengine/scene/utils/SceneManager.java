package com.oxidevelopment.bleakengine.scene.utils;

import com.oxidevelopment.bleakengine.scene.Scene;

public class SceneManager {
	
	private static Scene scene = null;
	
	/**
	 * Sets the current scene.
	 * @param scene The scene to change to.
	 */
	public static void setScene(Scene scene) {
		if(scene != null)
			scene.dispose();
		
		scene.loadContent();
		SceneManager.scene = scene;
	}
	
	/**
	 * Gets the current Scene.
	 * @return The current scene.
	 */
	public static Scene getScene() {
		return scene;
	}
	
}
